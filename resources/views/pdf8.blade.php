<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/pdf.css">
</head>
<body>
    <hr>
    <div class="mt-4 mb-10">
        <span style="font-size: 16px;" class="font-bold">Section 7: Demographic Information.</span>
        <span style="font-size: 12px;">This section asks about your ethnicity, sex, and race.</span>
    </div>

    <div class="border-bottom">
        <div style="font-size: 12px;" class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold">Demographic Information of Borrower</div>
    </div>

    <div style="font-size: 11px;" class="border-bottom leading-normal pb-4">
        <strong>The purpose of collecting this information</strong> is to help ensure that all applicants are treated fairly and that the housing needs of communities and neighborhoods are being fulfilled. For residential mortgage lending, Federal law requires that we ask applicants for their demographic information (ethnicity, sex, and race) in order to monitor our compliance with equal credit opportunity, fair housing, and home mortgage disclosure laws. You are not required to provide this information, but are encouraged to do so. You may select one or more designations for "Ethnicity" and one or more designations for "Race". <strong>The law provides that we may not discriminate</strong> on the basis of this information, or on whether you choose to provide it. However, if you choose not to provide the information and you have made this application in person, Federal regulations require us to note your ethnicity, sex, and race on the basis of visual observation or surname. The law also provides that we may not discriminate on the basis of age or marital status information you provide in this application. If you do not wish to provide some or all of this information, please check below.
    </div>

    <table style="font-size: 11px;">
        <tr>
            <td>
                <div>
                    <strong>Ethnicity:</strong>
                    <em>Check one or more</em>
                </div>
                <div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">Hispanic or Latino</span>
                </div>
                <div>
                    <div style="width: 25px;" class="inline-block"></div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">Mexican</span>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">Puerto Rican</span>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">Cuban</span>
                </div>
                <div>
                    <div style="width: 25px;" class="inline-block"></div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">Other Hispanic or Latino - <em>Print origin:</em></span>
                </div>
                <div>
                    <div style="width: 45px;" class="inline-block"></div>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4"></div>
                </div>
                <div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">Not Hispanic or Latino</span>
                </div>
                <div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">I do not wish to provide this information</span>
                </div>

                <div class="mt-16">
                    <strong>Sex:</strong>
                </div>
                <div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">Female</span>
                </div>
                <div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">Male</span>
                </div>
                <div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">I do not wish to provide this information</span>
                </div>
            </td>
            <td>
                <div>
                    <strong>Race:</strong>
                    <em>Check one or more</em>
                </div>
                <div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">American Indian or Alaska Native - <em>Print name of enrolled or principal tribe:</em></span>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4"></div>
                </div>
                <div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">Asian</span>
                </div>
                <div>
                    <div style="width: 20px;" class="inline-block"></div>
                    <input type="checkbox" class="align-middle">
                    <span style="width: 70px;" class="inline-block">Asian Indian</span>
                    <input type="checkbox" class="align-middle">
                    <span style="width: 70px;" class="inline-block">Chinese</span>
                    <input type="checkbox" class="align-middle">
                    <span style="width: 70px;" class="inline-block">Filipino</span>
                </div>
                <div>
                    <div style="width: 20px;" class="inline-block"></div>
                    <input type="checkbox" class="align-middle">
                    <span style="width: 70px;" class="inline-block">Japanese</span>
                    <input type="checkbox" class="align-middle">
                    <span style="width: 70px;" class="inline-block">Korean</span>
                    <input type="checkbox" class="align-middle">
                    <span style="width: 70px;" class="inline-block">Vietnamese</span>
                </div>
                <div>
                    <div style="width: 20px;" class="inline-block"></div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">Other Asian - <em>Print race:</em></span>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4"></div>
                </div>
                <div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">Black or African American</span>
                </div>
                <div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">Native Hawaiian or Other Pacific Islander</span>
                </div>
                <div>
                    <div style="width: 20px;" class="inline-block"></div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">Native Hawaiian</span>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">Guamanian or Chamorro</span>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">Samoan</span>
                </div>
                <div>
                    <div style="width: 20px;" class="inline-block"></div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">Other Pacific Islander - <em>Print race:</em></span>
                </div>
                <div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">White</span>
                </div>
                <div>
                    <input type="checkbox" class="align-middle">
                    <span class="inline-block">I do not wish to provide this information</span>
                </div>
            </td>
        </tr>
    </table>

    <div style="font-size: 11px;" class="mt-16 ">
        <div class="bg-gray font-bold p-4 border-top">To Be Completed by Financial Institution <em>(for application taken in person):</em></div>
        <div>
            <span style="width: 460px;" class="inline-block">Was the ethnicity of the Borrower collected on the basis of visual observation or surname?</span>
            <input type="radio" class="align-middle">
            <span class="inline-block mr-8">YES</span>
            <input type="radio" class="align-middle">
            <span class="inline-block">NO</span>
        </div>
        <div>
            <span style="width: 460px;" class="inline-block"> Was the sex of the Borrower collected on the basis of visual observation or surname?</span>
            <input type="radio" class="align-middle">
            <span class="inline-block mr-8">YES</span>
            <input type="radio" class="align-middle">
            <span class="inline-block">NO</span>
        </div>
        <div>
            <span style="width: 460px;" class="inline-block">Was the race of the Borrower collected on the basis of visual observation or surname?</span>
            <input type="radio" class="align-middle">
            <span class="inline-block mr-8">YES</span>
            <input type="radio" class="align-middle">
            <span class="inline-block">NO</span>
        </div>
    </div>

    <div style="font-size: 11px;" class="mb-20">
        <div class="bg-gray font-bold p-4 border-top">The Demographic Information was provided through:</div>
        <div>
            <input type="radio" class="align-middle">
            <span class="inline-block mr-8">Face-to-Face Interview <em>(includes Electronic Media w/ Video Component)</em></span>
            <input type="radio" class="align-middle">
            <span class="inline-block mr-8">Telephone Interview</span>
            <input type="radio" class="align-middle">
            <span class="inline-block mr-8">Fax or Mail</span>
            <input type="radio" class="align-middle">
            <span class="inline-block">Email or Internet</span>
        </div>
    </div>

    <hr>
    <div style="font-size: 11px;" class="mb-10 bg-gray p-4">
        <span style="font-size: 16px;" class="font-bold">Section 8: Loan Originator Information.</span>
        <div class="mt-16">
            <div style="font-size: 12px;" class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold">Loan Originator Information</div>
        </div>
        <div class="bg-white p-4">
            <div class="mb-4">
                <span>Loan Originator Organization Name</span>
                <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
            </div>
            <div class="mb-4">
                <span>Address</span>
                <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
            </div>
            <div class="mb-4">
                <span>Loan Originator Organization NMLSR ID#</span>
                <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                <span>State License ID#</span>
                <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
            </div>
            <div class="mb-4">
                <span>Loan Originator Name</span>
                <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
            </div>
            <div class="mb-4">
                <span>Loan Originator Organization NMLSR ID#</span>
                <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                <span>State License ID#</span>
                <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
            </div>
            <div class="mb-4">
                <span>Email</span>
                <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                <span>Phone</span>
                (<div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>)
                <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>-
                <div style="height: 18px; width: 80px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
            </div>
            <div class="mt-20">
                <span>Signature</span>
                <div style="height: 18px; width: 150px;" class="border-bottom inline-block align-middle px-4 leading-normal"></div>
                <span>Date <em>(mm/dd/yyyy)</em></span>
                <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>/
                <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>/
                <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
            </div>
        </div>
    </div>

    <hr>
    <div style="font-size: 12px;" class="mt-8">
        <strong>Borrower Name:</strong>
        <div style="height: 18px; width: 150px;" class="bg-blue inline-block px-4 mr-10"></div>
    </div>
    <div style="font-size: 10px;" class="pt-4">
        <div>Uniform Residential Loan Application</div>
        <div>Freddie Mac Form 65 • Fannie Mae Form 1003</div>
        <div><em>Effective 07/2019</em></div>
    </div>
</body>
</html 
