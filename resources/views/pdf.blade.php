<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/pdf.css">
</head>
<body>

    <div style="font-size: 9px;" class="py-4 px-4 bg-gray">
        <div><em>To be completed by the <strong>Lender:</strong></em></div>
        <div>
            <span>Lender Loan No./Universal Loan Identifier</span>
            <div style="width:30%;" class="inline-block border-bottom"></div>
            <span>Agency Case No.</span>
            <div style="width:20%;" class="inline-block border-bottom"></div>
        </div>
    </div>


    <div style="font-size: 16px;" class="font-bold mt-10 mb-4">Uniform Residential Loan Application</div>
    <div style="font-size: 10px;" class="mb-8"><strong>Verify and complete the information on this application.</strong> If you are applying for this loan with others, each additional Borrwer must provide information as directed by your Lender.</div>

    <hr>

    <div style="line-height: 0.9;" class="my-8">
        <span style="font-size: 16px;" class="font-bold">Section 1: Borrower Information.</span>
        <span style="font-size: 12px;">This section asks about your personal information and your income from employment and other sources, such as retirement, that you want considered to qualify for this loan.</span>
    </div>

    <table style="font-size: 11px; margin-bottom: 20px;" cellspacing="0">
        <tr>
            <td><div style="font-size: 12px;" class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold">1a. Personal Information</div></td>
        </tr>
        <tr>
            <td style="width: 55%;" class="border-top border-bottom border-right">
                <div class="font-bold">Name</div>
                <div class="bg-blue px-4">DAO LE MINH</div>
                <div class=""><strong>Alternate Names</strong><em> - List any names by which you are known or any names under which credit was previously received</em></div>
                <div style="height:18px;" class="bg-blue align-middle"></div>
            </td>
            <td style="width: 45%;" class="border-top pt-8 pl-4">
                <div class="">
                    <strong>Social Security Number</strong>
                    <div style="" class="bg-blue inline-block align-middle px-4">653</div>-
                    <div style="" class="bg-blue inline-block align-middle px-4">42</div>-
                    <div style="" class="bg-blue inline-block align-middle px-4">5161</div>
                </div>
                <table>
                    <tr>
                        <td style="width: 40%" class="">
                            <div class="mb-4">Date of Birth</div>
                            <div>
                                <div style="" class="bg-blue inline-block align-middle px-4">25</div>/
                                <div style="" class="bg-blue inline-block align-middle px-4">02</div>/
                                <div style="" class="bg-blue inline-block align-middle px-4">1990</div>
                            </div>
                        </td>
                        <td>
                            <div><strong>Citizenship</strong></div>
                            <div>
                                <input type="radio">
                                <div class="inline-block">U.S. Citizenship</div>
                            </div>
                            <div>
                                <input type="radio" checked>
                                <div class="inline-block">Permanent Resident Alien</div>
                            </div>
                            <div>
                                <input type="radio">
                                <div class="inline-block">Non-Permanent Resident Alien</div>
                            </div>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td class="border-bottom">
                <div><strong>Type of Credit</strong></div>
                <div>
                    <input type="radio">
                    <div class="inline-block">I'm applying for <strong>individual credit.</strong></div>
                </div>
                <div>
                    <input type="radio">
                    <div class="inline-block">
                        <span>I'm applying for <strong>joint credit.</strong> Total Number of Borrowers:</span>
                        <div style="height: 18px; width: 80px;" class="bg-blue inline-block align-middle px-4"></div>
                    </div>
                    <div style="margin-left: 10px;">
                        <span>Each Borrower intends to apply for join credit. <strong>Your Initials:</strong></span>
                        <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4"></div>
                    </div>
                </div>
            </td>
            <td class="border-top">
                <div><strong>List Name(s) of Other Borrower(s) Applying for this Loan</strong></div>
                <div style="height: 18px; width: 100%;" class="bg-blue inline-block align-middle px-4"></div>
            </td>
        </tr>
        <tr>
            <td class="border-bottom border-right">
                <table>
                    <tr>
                        <td style="width: 40%;">
                            <div><strong>Marital Status</strong></div>
                            <div>
                                <div class="inline-block"><input type="radio"></div>
                                <div class="inline-block">Married</div>
                            </div>
                            <div>
                                <div class="inline-block"><input type="radio"></div>
                                <div class="inline-block">Separated</div>
                            </div>
                            <div>
                                <div class="inline-block"><input type="radio"></div>
                                <div class="inline-block">Unmarried</div>
                            </div>
                        </td>
                        <td class="">
                            <div><strong>Dependents</strong></div>
                            <div>
                                <span class="inline-block">Number</span>
                                <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4"></div>
                            </div>
                            <div>
                                <span class="inline-block">Ages</span>
                                <div style="height: 18px; width: 200px;" class="bg-blue inline-block align-middle px-4"></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="border-top pl-4">
                <div><strong>Contact Information</strong></div>
                <div>
                    <div style="width: 75px;" class="inline-block"><strong>Home</strong> Phone</div>
                    (<div style="height: 18px; width: 20px;" class="bg-blue inline-block align-middle px-4"></div>)
                    <div style="height: 18px; width: 20px;" class="bg-blue inline-block align-middle px-4"></div> -
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4"></div>
                </div>
                <div>
                    <div style="width: 75px;" class="inline-block"><strong>Cell</strong> Phone</div>
                    (<div style="height: 18px; width: 20px;" class="bg-blue inline-block align-middle px-4"></div>)
                    <div style="height: 18px; width: 20px;" class="bg-blue inline-block align-middle px-4"></div> -
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4"></div>
                </div>
                <div>
                    <div style="width: 75px;" class="inline-block"><strong>Work</strong> Phone</div>
                    (<div style="height: 18px; width: 20px;" class="bg-blue inline-block align-middle px-4"></div>)
                    <div style="height: 18px; width: 20px;" class="bg-blue inline-block align-middle px-4"></div> -
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4"></div>
                    <div style="" class="ml-8 inline-block align-middle">Ext. </div>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4"></div>
                </div>
                <div>
                    <span class="font-bold">Email</span>
                    <div style="height: 18px; width: 80%;" class="bg-blue inline-block align-middle px-4"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="border-top border-bottom">
                <div><strong>Current Address</strong></div>
                <div>
                    <span>Street</span>
                    <div style="height: 18px; width: 70%" class="bg-blue inline-block align-middle px-4"></div>
                    <span>Unit</span>
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4"></div>
                </div>
                <div>
                    <span>City</span>
                    <div style="height: 18px; width: 80px;" class="bg-blue inline-block align-middle px-4"></div>
                    <span>State</span>
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4"></div>
                    <span>ZIP</span>
                    <div style="height: 18px; width: 60px;" class="bg-blue inline-block align-middle px-4"></div>
                    <span>Country</span>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4"></div>
                </div>
                <div>
                    <span style="">How long at Current Address?</span>
                    <div style="" class="bg-blue inline-block align-middle px-4">05</div>
                    <span style="">Years</span>
                    <div style="" class="bg-blue inline-block align-middle px-4">00</div>
                    <span style="">Months</span>
                    <span class="font-bold">|Housing:</span>
                    <input style="font-size: 14px;" type="radio">
                    <span>No primary housing expense</span>
                    <input style="font-size: 14px;" class="align-middle" type="radio">
                    <span>Own</span>
                    <input style="font-size: 14px;" class="align-middle" type="radio">
                    <span>Rent</span>
                    <span>($</span>
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4"></div>
                    <span>/month)</span>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="border-bottom">
                <div>
                    <strong>If at Current Address for LESS than 2 years, list Former Address</strong>
                    <input class="align-middle" type="checkbox">
                    <span>Does not apply</span>
                </div>
                <div>
                    <span>Street</span>
                    <div style="height: 18px; width: 70%" class="bg-blue inline-block align-middle px-4"></div>
                    <span>Unit</span>
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4"></div>
                </div>
                <div>
                    <span>City</span>
                    <div style="height: 18px; width: 80px;" class="bg-blue inline-block align-middle px-4"></div>
                    <span>State</span>
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4"></div>
                    <span>ZIP</span>
                    <div style="height: 18px; width: 60px;" class="bg-blue inline-block align-middle px-4"></div>
                    <span>Country</span>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4"></div>
                </div>
                <div>
                    <span>How long at Current Address?</span>
                    <div class="bg-blue inline-block align-middle px-4">05</div>
                    <span>Years</span>
                    <div class="bg-blue inline-block align-middle px-4">00</div>
                    <span>Months</span>
                    <span class="font-bold">|Housing:</span>
                    <input style="font-size: 14px;" class="align-middle" type="radio">
                    <span>No primary housing expense</span>
                    <input style="font-size: 14px;" class="align-middle" type="radio">
                    <span>Own</span>
                    <input style="font-size: 14px;" class="align-middle" type="radio">
                    <span>Rent</span>
                    <span>($</span>
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4"></div>
                    <span>/month)</span>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="border-top border-bottom">
                <div>
                    <strong>Mailing Address - if different from Current Address</strong>
                    <input class="align-middle" type="checkbox">
                    <span>Does not apply</span>
                </div>
                <div>
                    <span>Street</span>
                    <div style="height: 18px; width: 70%" class="bg-blue inline-block align-middle px-4"></div>
                    <span>Unit</span>
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4"></div>
                </div>
                <div>
                    <span>City</span>
                    <div style="height: 18px; width: 80px;" class="bg-blue inline-block align-middle px-4"></div>
                    <span>State</span>
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4"></div>
                    <span>ZIP</span>
                    <div style="height: 18px; width: 60px;" class="bg-blue inline-block align-middle px-4"></div>
                    <span>Country</span>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4"></div>
                </div>
                <div>
                    <span>How long at Current Address?</span>
                    <div style="" class="bg-blue inline-block align-middle px-4">05</div>
                    <span>Years</span>
                    <div style="" class="bg-blue inline-block align-middle px-4">00</div>
                    <span>Months</span>
                    <span class="font-bold">|Housing:</span>
                    <input style="font-size: 14px;" class="align-middle" type="radio">
                    <span>No primary housing expense</span>
                    <input style="font-size: 14px;" class="align-middle" type="radio">
                    <span>Own</span>
                    <input style="font-size: 14px;" class="align-middle" type="radio">
                    <span>Rent</span>
                    <span>($</span>
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4"></div>
                    <span>/month)</span>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="border-bottom">
                <div>
                    <div class="inline-block">
                        <strong>Military Service</strong>
                        <span> - Did you (or your deceased spouse) ever serve, or are you currently serving, in the United States Armed Forces?</span>
                    </div>
                    <div class="inline-block"><input type="radio"></div>
                    <div class="inline-block font-bold">NO</div>
                    <div class="inline-block"><input type="radio" checked></div>
                    <div class="inline-block font-bold">YES</div>
                </div>
                <table>
                    <tr>
                        <td class="align-top">
                            If YES, check all that apply:
                        </td>
                        <td>
                            <div style="margin-bottom: -8px;">
                                <div class="inline-block"><input type="checkbox"></div>
                                <div class="inline-block">Currently serving on active duty with projected expiration date of service/tour</div>
                                <div style="height: 18px; width: 20px;" class="bg-blue inline-block align-middle px-4"></div>/
                                <div style="height: 18px; width: 30px;" class="bg-blue inline-block align-middle px-4"></div>(mm/yyyy)
                            </div>
                            <div style="margin-bottom: -8px;">
                                <div class="inline-block"><input type="checkbox"></div>
                                <div class="inline-block">Currently retired, discharged, or seperated from service</div>
                            </div>
                            <div style="margin-bottom: -8px;">
                                <div class="inline-block"><input type="checkbox"></div>
                                <div class="inline-block">Only period of service was as a non-activated member of the Reserve or National Guard</div>
                            </div>
                            <div style="margin-bottom: -8px;">
                                <div class="inline-block"><input type="checkbox"></div>
                                <div class="inline-block">Surviving spouse</div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="pt-4">
                    <strong>Language Preference - Your loan transaction is likely to be conducted in English.</strong>
                    <span> This question requests information to see if communications are avaiable to assist you in your perferred language. Please be aware that communications may NOT be avaiable in your perferred language.</span>
                </div>
                <div>
                    <em>Optional</em>
                    <span> - Mark the language you would prefer, if avaiable:</span>
                </div>
                <div>
                    <div class="inline-block"><input type="radio"></div>
                    <div class="inline-block mr-10"><span>English</span></div>
                    <div class="inline-block"><input type="radio"></div>
                    <div class="inline-block mr-10"><span>Chinese</span></div>
                    <div class="inline-block"><input type="radio"></div>
                    <div class="inline-block mr-10"><span>Korean</span></div>
                    <div class="inline-block"><input type="radio"></div>
                    <div class="inline-block mr-10"><span>Spanish</span></div>
                    <div class="inline-block"><input type="radio"></div>
                    <div class="inline-block mr-10"><span>Tagalog</span></div>
                    <div class="inline-block"><input type="radio"></div>
                    <div class="inline-block mr-10"><span>Vietnamese</span></div>
                    <div class="inline-block"><input type="radio"></div>
                    <div class="inline-block"><span>Other:</span></div>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 mr-10"></div>
                    <div class="inline-block"><input type="radio"></div>
                    <div class="inline-block"><span>I do not wish to respond</span></div>
                </div>
                <div>
                    Your answer will NOT negatively affect your mortgage application. Your answer does not mean the Lender or other Loan Participants agree to communicate or provide documents in your preferred language. However, if may let them assist you or direct you to persons who can assist you.
                </div>
                <div>
                    Language assistance and resources may be available through housing counseling agencies approved by the U.S. Department of Housing and Urban Development. To find a housing counseling agency, contact one of the following Federal government agencies:
                </div>
                <div class="mt-4">
                    <ul style="margin-left: 30px;">
                        <li>U.S. Department of Housing and urban Development (HUD) at (800) 569-4287 or <span class="underline">www.hud.gov/counseling</span>.</li>
                        <li>Consumer Financial Protection Bureau (CFPB) at (855) 411-2372 or <span class="underline">www.consumer.gov/find-a-housing-counselor</span>.</li>
                    </ul>
                </div>
            </td>
        </tr>
    </table>

    <hr>

    <div style="font-size: 10px;" class="pt-4">
        <div>Uniform Residential Loan Application</div>
        <div>Freddie Mac Form 65 • Fannie Mae Form 1003</div>
        <div><em>Effective 07/2019</em></div>
    </div>

</body>
</html>
