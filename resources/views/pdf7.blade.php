<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/pdf.css">
</head>
<body>
    <hr>
    <div class="mt-4 mb-10">
        <span style="font-size: 16px;" class="font-bold">Section 6: Acknowledgements and Agreements.</span>
        <span style="font-size: 12px;">This section tells you about your legal obligations when you sign this application.</span>
    </div>

    <div class="border-bottom">
        <div style="font-size: 12px;" class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold">Acknowledgements and Agreements</div>
    </div>

    <table style="font-size: 11px;" cellpadding="0">
        <tr>
            <td>
                <div>I agree to, acknowledge, and represent the following statements to:</div>
                <div>• The Lender (this includes the Lender's agents, service providers and any of their successors and assigns); AND</div>
                <div>• Other Loan Participants (this includes any actual or potential owners of a loan resulting from this application (the"Loan"), or acquirers of any beneficial or other interest in the Loan, any mortgage insurer, guarantor, any servicers or service providers of the Loan, and any of their successors and assigns).</div>
                <div class="font-bold mt-8">By signing below, I agree to, acknowledge, and represent the following statements about:</div>
                <div class="font-bold mt-4">(1) The Complete Information for this Application</div>
                <div>• The information I have provided in this application is true, accurate, and complete as of the date I signed this application.</div>
                <div>• If the information I submitted changes or I have new information before closing of the Loan, I must change and supplement this application or any real estate sales contract, including providing any updated/supplemented real estate sales contract.</div>
                <div>• For purchase transactions: The terms and conditions of any real estate sales contract signed by me in connection with this application are true, accurate, and complete to the best of my knowledge and belief. I have not entered into any other agreement, written or oral, in connection with this real estate transaction.</div>
                <div>• The Lender and Other Loan Participants may rely on the information contained in the application before and after closing of the Loan.</div>
                <div>• Any intentional or negligent misrepresentation of information may result in the imposition of:</div>
                <table cellpadding="0">
                    <tr>
                        <td style="width: 20px;"></td>
                        <td>
                            <div>(a) civil liability on me, including monetary damages, if a person suffers any loss because the person relied on any misrepresentation that I have made on this application, and/or</div>
                            <div>(b) criminal penalties on me including, but not limited to, fine or imprisonment or both under the provisions of federal law (18 U.S.C. 5§ 1001 et seq.).</div>
                        </td>
                    </tr>
                </table>
                <div class="font-bold mt-4">(2) The Property's Security</div>
                <div>•The Loan I have applied for in this application will be secured by a mortgage or deed of trust which provides the Lender a security interest in the property described in this application.</div>
            </td>
            <td>
                <div class="font-bold">(3) The Property's Appraisal, Value, and Condition</div>
                <div>• Any appraisal or value of the property obtained by the Lender is for use by the Lender and Other Loan Participants.</div>
                <div>• The Lender and Other Loan Participants have not made any representation or warranty, express or implied, to me about the property, its condition, or its value.</div>
                <div class="font-bold mt-4">(4) Electronic Records and Signatures</div>
                <div>• The Lender and Other Loan Participants may keep any paper record and/or electronic record of this application, whether or not the Loan is approved.</div>
                <div>• If this application is created as (or converted into) an"electronic application", I consent to the use of"electronic records"and "electronic signatures"as the terms are defined in and governed by applicable federal and/or state electronic transactions laws.</div>
                <div>• I intend to sign and have signed this application either using my: (a) electronic signature; or (b) a written signature and agree that if a paper version of this application is converted into an electronic application, the application will be an electronic record, and the representation of my written signature on this application will be my binding electronic signature.</div>
                <div>• I agree that the application, if delivered or transmitted to the Lender or Other Loan Participants as an electronic record with my electronic signature, will be as effective and enforceable as a paper application signed by me in writing.</div>
                <div class="font-bold mt-4">(5) Delinquency</div>
                <div>•The Lender and Other Loan Participants may report information about my account to credit bureaus. Late payments, missed payments, or other defaults on my account may be reflected in my credit report and will likely affect my credit score.</div>
                <div>• If I have trouble making my payments I understand that I may contact a HUD-approved housing counseling organization for advice about actions I can take to meet my mortgage obligations.</div>
                <div class="font-bold mt-4">(6) Use and Sharing of Information</div>
                <div>I understand and acknowledge that the Lender and Other Loan Participants can obtain, use, and share the loan application, a consumer credit report, and related documentation for purposes permitted by applicable laws.</div>
            </td>
        </tr>
    </table>

    <div style="font-size: 11px; margin-top: 100px;">
        <div class="inline-block font-bold">Borrower Signature</div>
        <div style="width: 350px;" class="inline-block border-bottom"></div>
        <div class="inline-block">Date <em>(mm/dd/yyyy)</em></div>
        <div style="height: 18px; width: 30px;" class="bg-blue inline-block px-4"></div>/
        <div style="height: 18px; width: 30px;" class="bg-blue inline-block px-4"></div>/
        <div style="height: 18px; width: 50px;" class="bg-blue inline-block px-4"></div>
    </div>

    <div style="font-size: 11px; margin-top: 50px;">
        <div class="inline-block font-bold">Borrower Signature</div>
        <div style="width: 350px;" class="inline-block border-bottom"></div>
        <div class="inline-block">Date <em>(mm/dd/yyyy)</em></div>
        <div style="height: 18px; width: 30px;" class="bg-blue inline-block px-4"></div>/
        <div style="height: 18px; width: 30px;" class="bg-blue inline-block px-4"></div>/
        <div style="height: 18px; width: 50px;" class="bg-blue inline-block px-4"></div>
    </div>

    <hr style="margin-top: 150px;">

    <div style="font-size: 10px;" class="pt-4">
        <div>Uniform Residential Loan Application</div>
        <div>Freddie Mac Form 65 • Fannie Mae Form 1003</div>
        <div><em>Effective 07/2019</em></div>
    </div>
</body>
</html>