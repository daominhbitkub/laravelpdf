<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/pdf.css">
</head>
<body>
    <hr>
    <div class="mt-4 mb-10">
        <span style="font-size: 16px;" class="font-bold">Section 4: Loan and Property Information.</span>
        <span style="font-size: 12px;">This section asks about the loan's purpose and the property you want to purchase or refinance.</span>
    </div>

    <div class="border-bottom">
        <div style="font-size: 12px;" class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold">
            4a. Loan and Property Information
        </div>
    </div>

    <table style="font-size: 11px;" class="bordered mb-20" cellspacing="0">
        <tr>
            <td>
                <div>
                    <strong>Loan Amount $</strong>
                    <div style="height: 18px; width: 110px; margin-right: 30px" class="bg-blue inline-block px-4"></div>
                    <div class="inline-block font-bold mr-8">Loan Purpose</div>
                    <input style="font-size: 14px;" type="radio" checked>
                    <span class="inline-block mr-8">Purchase</span>
                    <input style="font-size: 14px;" type="radio">
                    <span class="inline-block mr-8">Refinance</span>
                    <input style="font-size: 14px;" type="radio">
                    <span class="inline-block mr-8">Other <em>(specify)</em></span>
                    <div style="height: 18px; width: 110px;" class="bg-blue inline-block px-4"></div>
                </div>
                <div>
                    <div style="width: 100px;" class="inline-block"><strong>Property Address</strong></div>
                    <div class="inline-block">Street</div>
                    <div style="height: 18px; width: 470px;" class="bg-blue inline-block px-4"></div>
                    <div class="inline-block">Unit #</div>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block px-4"></div>
                </div>
                <div>
                    <div style="width: 100px;" class="inline-block"></div>
                    <div class="inline-block">City</div>
                    <div style="height: 18px; width: 400px;" class="bg-blue inline-block px-4"></div>
                    <div class="inline-block">State</div>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block px-4"></div>
                    <div class="inline-block">ZIP</div>
                    <div style="height: 18px; width: 70px;" class="bg-blue inline-block px-4"></div>
                </div>
                <div>
                    <div style="width: 100px;" class="inline-block"></div>
                    <div class="inline-block">Country</div>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block px-4 mr-10"></div>
                    <div class="inline-block">Number of Units</div>
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block px-4 mr-10"></div>
                    <div class="inline-block"><strong>Property Value</strong> $</div>
                    <div style="height: 18px; width: 100px;" class="bg-blue inline-block px-4"></div>
                </div>
                <div>
                    <div style="width: 100px;" class="inline-block"><strong>Occupancy</strong></div>
                    <input style="font-size: 14px;" type="radio" checked>
                    <div class="inline-block mr-16">Primary Residence</div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block mr-16">Second Home</div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block mr-16">Investment Property</div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block mr-16">FHA Secondary Residence</div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <div style="width: 15px;" class="inline-block font-bold">1.</div>
                    <div class="inline-block font-bold">Mixed-up Property.</div>
                    <div class="inline-block">If you will occupy the property, will you set aside space within the property to operate your own business?</div>
                </div>
                <div>
                    <div style="width: 15px;" class="inline-block font-bold"></div>
                    <div class="inline-block mr-16"><em>(e.g., daycare facility, medical office, beauty/barber shop)</em></div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block mr-8">NO</div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block">YES</div>
                </div>
                <div>
                    <div style="width: 15px;" class="inline-block font-bold">2.</div>
                    <div class="inline-block"><strong>Manufactured Home.</strong></div>
                    <div class="inline-block mr-16">Is the property a manufactured home?<em> (e.g., a factory built dwelling built on a permanent chassis)</em></div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block mr-8">NO</div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block">YES</div>
                </div>
            </td>
        </tr>
    </table>

    <div class="border-bottom">
        <div style="font-size: 12px;" class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold">
            4b. Other New Mortgage Loans on the Property You are Buying or Refinancing
        </div>
        <div style="font-size: 11px;" class="inline-block ml-8">
            <input type="checkbox" class="align-bottom">
            <span class="font-bold italic">Does not apply</span>
        </div>
    </div>

    <table style="font-size: 11px;" class="bordered mb-20" cellspacing="0">
        <tr>
            <td><strong>Creditor Name</strong></td>
            <td style="width: 180px;"><strong>Lien Type</strong></td>
            <td><strong>Monthly Payment</strong></td>
            <td><strong>Loan Amount/<br>Amount to be Drawn</strong></td>
            <td>
                <strong>Credit Limit</strong><br>
                <em>(if applicable)</em>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type="radio">
                <div class="inline-block mr-8">First Lien</div>
                <input type="radio">
                <div class="inline-block">Subordinate Lien</div>
            </td>
            <td>$</td>
            <td>$</td>
            <td>$</td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type="radio">
                <div class="inline-block mr-8">First Lien</div>
                <input type="radio">
                <div class="inline-block">Subordinate Lien</div>
            </td>
            <td>$</td>
            <td>$</td>
            <td>$</td>
        </tr>
    </table>

    <div class="border-bottom">
        <div style="font-size: 12px;" class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold">
            4c. Rental Income on the Property You Want to Purchase
        </div>
        <div style="font-size: 11px;" class="inline-block ml-8">
            <span class="font-bold">For Purchase Only</span>
            <div style="width: 8px;" class="inline-block"></div>
            <input type="checkbox" class="align-bottom">
            <span class="font-bold italic">Does not apply</span>
        </div>
    </div>

    <table style="font-size: 11px;" class="bordered mb-20" cellspacing="0">
        <tr>
            <td><strong>Complete if the property is a 2-4 Unit Primary Residence or an Investment Property</strong></td>
            <td><strong>Amount</strong></td>
        </tr>
        <tr>
            <td>Expected Monthly Rental Income</td>
            <td>$</td>
        </tr>
        <tr>
            <td>
                <strong>For LENDER to calculate:</strong>
                <span>Expected Net Monthly Rental Income</span>
            </td>
            <td>$</td>
        </tr>
    </table>

    <div class="border-bottom">
        <div style="font-size: 12px;" class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold">
            4d. Gifts or Grants You Have Been Given or Will Receive for this Loan
        </div>
        <div style="font-size: 11px;" class="inline-block ml-8">
            <input type="checkbox" class="align-bottom">
            <span class="font-bold italic">Does not apply</span>
        </div>
    </div>

    <table style="font-size: 11px;" class="bordered mb-20" cellspacing="0">
        <tr>
            <td colspan="4">
                <div class="font-bold">Include all gifts and grants below. Under Source, choose from the sources listed here:</div>
                <table style="width: auto;" class="unbordered">
                    <tr>
                        <td>
                            <div>• Relative</div>
                            <div>• Unmarried Partner</div>
                        </td>
                        <td>
                            <div>• Employer</div>
                            <div>• Religious Nonprofit</div>
                        </td>
                        <td>
                            <div>• Community Nonprofit</div>
                            <div>• Federal Agency</div>
                        </td>
                        <td>
                            <div>• State Agency</div>
                            <div>• Local Agency</div>
                        </td>
                        <td class="align-top">
                            <div>• Other</div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Asset Type:</strong>
                <span>Cash Gift, Gift or Equity, Grant</span>
            </td>
            <td><strong>Deposited/Not Deposited</strong></td>
            <td>
                <strong>Source</strong>
                <em>- use list above</em>
            </td>
            <td><strong>Cash or Market Value</strong></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type="radio">
                <div class="inline-block mr-8">First Lien</div>
                <input type="radio">
                <div class="inline-block">Subordinate Lien</div>
            </td>
            <td></td>
            <td>$</td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type="radio">
                <div class="inline-block mr-8">First Lien</div>
                <input type="radio">
                <div class="inline-block">Subordinate Lien</div>
            </td>
            <td></td>
            <td>$</td>
        </tr>
    </table>

    <hr style="margin-top: 230px;">
    <div style="font-size: 12px;" class="mt-8">
        <strong>Borrower Name:</strong>
        <div style="height: 18px; width: 150px;" class="bg-blue inline-block px-4 mr-10"></div>
    </div>
    <div style="font-size: 10px;" class="pt-4">
        <div>Uniform Residential Loan Application</div>
        <div>Freddie Mac Form 65 • Fannie Mae Form 1003</div>
        <div><em>Effective 07/2019</em></div>
    </div>
</body>
</html>