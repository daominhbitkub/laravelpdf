<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/pdf.css">
</head>
<body>
    <hr>
    <div class="mb-10">
        <span style="font-size: 16px;" class="font-bold">Section 2: Financial Information - Assets and Liabilities.</span>
        <span style="font-size: 12px;">This section asks about things you own that are worth money and that you want considered to qualify for this loan. It then asks about your liabilities (or debts) that you pay each month, such as credit cards, alimony, or other expenses.</span>
    </div>

    <div class="border-bottom">
        <div style="font-size: 12px;" class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold">
            2a. Assets - Bank Accounts, Retirement, and Other Accounts You Have
        </div>
    </div>

    <div style="font-size: 11px;"><strong>Include all accounts below. Under Account Type, choose from the types listed here:</strong></div>

    <table style="font-size: 10px;">
        <tr>
            <td style="padding-left: 9px;">
                <ul>
                    <li>Checking</li>
                    <li>Savings</li>
                    <li>Money Market</li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>Certificate Deposit</li>
                    <li>Mutual Fund</li>
                    <li>Stocks</li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>Stock Options</li>
                    <li>Bonds</li>
                    <li>Retirement <em>(e.g, 401k, IRA)</em></li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>Bridge Loan Proceeds</li>
                    <li>Individual Development</li>
                    <li>Account</li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>Trust Account</li>
                    <li>Cash Value of Life Insurance <br>(used for transaction)</li>
                </ul>
            </td>
        </tr>
    </table>

    <table style="font-size: 11px;" class="bordered mb-20" cellspacing="0">
        <tr>
            <td>
                <strong>Account Type</strong>
            </td>
            <td>
                <strong>Financial Institution</strong>
            </td>
            <td>
                <strong>Account Number</strong>
            </td>
            <td>
                <strong>Cash or Market Value</strong>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>$</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>$</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>$</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>$</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>$</td>
        </tr>
        <tr>
            <td class="text-right font-bold" colspan="3">Provide TOTAL Amount Here</td>
            <td><strong>$</strong></td>
        </tr>
    </table>
    
    <div style="font-size: 12px;" class="border-bottom">
        <div class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold mr-8">
            2b. Other Assets You Have
        </div>
        <input type="checkbox">
        <div class="inline-block"><strong>Does not apply</strong></div>
    </div>

    <div style="font-size: 11px;"><strong>Include all other assets below. Under Asset Type, choose from the types listed here:</strong></div>

    <table style="font-size: 10px;">
        <tr>
            <td style="padding-left: 9px;">
                <ul>
                    <li>Earnest Money</li>
                    <li>Proceeds from Sale of<br>Non-Real Estate Asset</li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>Proceeds from Real Estate Property<br>to be sold or before closing</li>
                    <li>Sweat Equity</li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>Employer Assistance</li>
                    <li>Rent Credit</li>
                    <li>Secured Borrowed Funds</li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>Trade Equity</li>
                    <li>Unsecured Borrowed Funds</li>
                    <li>Other</li>
                </ul>
            </td>
        </tr>
    </table>

    <table style="font-size: 11px;" class="bordered mb-20" cellspacing="0">
        <tr>
            <td style="width: 80%;"><strong>Asset Type</strong></td>
            <td><strong>Cash or Market Value</strong></td>
        </tr>
        <tr>
            <td style="width: 80%;"></td>
            <td>$</td>
        </tr>
        <tr>
            <td style="width: 80%;"></td>
            <td>$</td>
        </tr>
        <tr>
            <td style="width: 80%;"></td>
            <td>$</td>
        </tr>
        <tr>
            <td style="width: 80%;" class="text-right"><strong>Provide TOTAL Amount Here</strong></td>
            <td><strong>$</strong></td>
        </tr>
    </table>

    <div style="font-size: 12px;" class="border-bottom">
        <div class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold mr-8">
            2c. Liabilities - Credit Cards, Other Debts, and leases that You Owe
        </div>
        <input type="checkbox">
        <div class="inline-block"><strong>Does not apply</strong></div>
    </div>

    <div style="font-size: 11px;"><strong>List all liabilities below (except real estate) and include deferred payments. Under Account Type, choose from the types listed here:</strong></div>
    <table style="font-size: 10px;">
        <tr>
            <td>• Revolving <em>(e.g., credit cards)</em></td>
            <td>• Installment <em>(e.g., car, student, personal loans)</em></td>
            <td>• Open 30-Day <em>(balance paid monthly)</em></td>
            <td>• Lease <em>(not real estate)</em></td>
            <td>• Other</td>
        </tr>
    </table>

    <table style="font-size: 11px;" class="bordered mb-20" cellspacing="0">
        <tr>
            <td><strong>Account Type</strong></td>
            <td><strong>Company Name</strong></td>
            <td><strong>Account Number</strong></td>
            <td>
                <table cellspacing="0">
                    <tr>
                        <td class="border-none p-0"><strong>Unpaid Balance</strong></td>
                        <td class="border-none p-0"><em>(To be paid off at<br>or before closing)</em></td>
                    </tr>
                </table>
            </td>
            <td><strong>Monthly Payment</strong></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <table cellspacing="0">
                    <tr>
                        <td class="border-none">$</td>
                        <td class="border-none"><input style="line-height: 0.7;" type="checkbox" class="inline-block align-bottom"></td>
                    </tr>
                </table>
            </td>
            <td>$</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <table cellspacing="0">
                    <tr>
                        <td class="border-none">$</td>
                        <td class="border-none"><input style="line-height: 0.7;" type="checkbox" class="inline-block align-bottom"></td>
                    </tr>
                </table>
            </td>
            <td>$</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <table cellspacing="0">
                    <tr>
                        <td class="border-none">$</td>
                        <td class="border-none"><input style="line-height: 0.7;" type="checkbox" class="inline-block align-bottom"></td>
                    </tr>
                </table>
            </td>
            <td>$</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="border-none">$</td>
                        <td class="border-none"><input style="line-height: 0.7;" type="checkbox" class="inline-block align-middle"></td>
                    </tr>
                </table>
            </td>
            <td>$</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <table cellspacing="0">
                    <tr>
                        <td class="border-none">$</td>
                        <td class="border-none"><input style="line-height: 0.7;" type="checkbox" class="inline-block align-bottom"></td>
                    </tr>
                </table>
            </td>
            <td>$</td>
        </tr>
    </table>

    <div style="font-size: 12px;" class="border-bottom">
        <div class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold mr-8">
            2d. Other Liabilities and Expenses
        </div>
        <input type="checkbox">
        <div class="inline-block"><strong>Does not apply</strong></div>
    </div>

    <table style="font-size: 11px;" class="bordered mb-20" cellspacing="0">
        <tr>
            <td>
                <div class="mb-4"><strong>Include all other liabilities and expenses below. Choose from the types listed here:</strong></div>
                <div style="font-size: 10px;">
                    <div class="inline-block mr-10">• Alimony</div>
                    <div class="inline-block mr-10">• Child Support</div>
                    <div class="inline-block mr-10">• Separate Maintenance</div>
                    <div class="inline-block mr-10">• Job Related Expenses</div>
                    <div class="inline-block">• Other</div>
                </div>
            </td>
            <td>
                <strong>Monthly Payment</strong>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>$</td>
        </tr>
        <tr>
            <td></td>
            <td>$</td>
        </tr>
        <tr>
            <td></td>
            <td>$</td>
        </tr>
    </table>

    <hr>
    <div style="font-size: 12px;" class="mt-8">
        <strong>Borrower Name:</strong>
        <div style="height: 18px; width: 150px;" class="bg-blue inline-block px-4 mr-10"></div>
    </div>
    <div style="font-size: 10px;" class="pt-4">
        <div>Uniform Residential Loan Application</div>
        <div>Freddie Mac Form 65 • Fannie Mae Form 1003</div>
        <div><em>Effective 07/2019</em></div>
    </div>
</body>
</html>
