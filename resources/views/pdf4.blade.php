<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/pdf.css">
</head>
<body>
    <hr>
    <div class="mt-4 mb-10">
        <span style="font-size: 16px;" class="font-bold">Section 3: Financial Information - Real Estate.</span>
        <span style="font-size: 12px;">This section asks you to list all properties you currently own and what you owe on them.</span>
        <input style="vertical-align: middle; font-size: 10px;" class="" type="checkbox">
        <span style="font-size: 10px;" class="font-bold italic">I do not own any real estate</span>
    </div>

    <div class="border-bottom">
        <div style="font-size: 12px;" class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold">
            3a. Property You Own
        </div>
        <div style="font-size: 11px;" class="inline-block ml-8"><strong>If you are refinancing, list the property you are refinancing FIRST.</strong></div>
    </div>

    <table style="font-size: 11px;" class="bordered" cellspacing="0">
        <tr>
            <td colspan="5">
                <div class="mb-4"><strong>Address</strong></div>
                <div>
                    <span>Street</span>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block px-4"></div>
                    <span>Unit #</span>
                    <div style="height: 18px; width: 30px;" class="bg-blue inline-block px-4"></div>
                    <span>City</span>
                    <div style="height: 18px; width: 100px;" class="bg-blue inline-block px-4"></div>
                    <span>State</span>
                    <div style="height: 18px; width: 20px;" class="bg-blue inline-block px-4"></div>
                    <span>ZIP</span>
                    <div style="height: 18px; width: 70px;" class="bg-blue inline-block px-4"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td rowspan="2">
                <strong>Property Value</strong>
            </td>
            <td rowspan="2">
                <strong>Status:</strong>
                <span>Sold, Pending <br>Sale, or Retained</span>
            </td>
            <td rowspan="2">
                <strong>Monthly Insurance, Taxes, Association Dues, etc.</strong><br>
                <em>if not included in <br>Monthly Mortgage Payment</em>
            </td>
            <td colspan="2" class="text-center">
                <strong>For Investment property Only</strong>
            </td>
        </tr>
        <tr>
            <td><strong>Monthly Rental Income</div></td>
            <td>
                <strong>For LENDER to calculate:</strong><br>
                <span>Net Monthly Rental Income</span>
            </td>
        </tr>
        <tr>
            <td>$</td>
            <td></td>
            <td>$</td>
            <td>$</td>
            <td>$</td>
        </tr>
    </table>
    <div style="font-size: 10px;">
        <div class="inline-block font-bold">Mortgage Loans on this Property</div>
        <div class="inline-block ml-8">
            <input type="checkbox" class="align-bottom">
            <span class="font-bold italic">Does not apply</span>
        </div>
    </div>
    <table style="font-size: 11px;" class="bordered mb-20" cellspacing="0">
        <tr>
            <td><strong>Creditor Name</strong></td>
            <td><strong>Account Number</strong></td>
            <td><strong>Monthly <br>Mortgage <br>Payment</strong></td>
            <td>
                <table cellspacing="0">
                    <tr>
                        <td class="border-none p-0"><strong>Unpaid Balance</strong></td>
                        <td class="border-none p-0"><em>(To be paid off at<br>or before closing)</em></td>
                    </tr>
                </table>
            </td>
            <td>
                <strong>Type:</strong>
                <span>FHA, VA, <br>Conventional, <br>USDA-RD, Other</span>
            </td>
            <td>
                <strong>Credit Limit</strong><br>
                <em>(if applicable)</em>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>$</td>
            <td><table cellspacing="0">
                <tr>
                    <td class="border-none">$</td>
                    <td class="border-none"><input style="line-height: 0.7;" type="checkbox" class="inline-block align-bottom"></td>
                </tr>
            </table></td>
            <td></td>
            <td>$</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>$</td>
            <td><table cellspacing="0">
                <tr>
                    <td class="border-none">$</td>
                    <td class="border-none"><input style="line-height: 0.7;" type="checkbox" class="inline-block align-bottom"></td>
                </tr>
            </table></td>
            <td></td>
            <td>$</td>
        </tr>
    </table>

    <div class="border-bottom">
        <div style="font-size: 12px;" class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold">
            3b. IF APPLICABLE, Complete Information for Additional Property
        </div>
        <div style="font-size: 11px;" class="inline-block ml-8">
            <input type="checkbox" class="align-bottom">
            <span class="font-bold italic">Does not apply</span>
        </div>
    </div>

    <table style="font-size: 11px;" class="bordered" cellspacing="0">
        <tr>
            <td colspan="5">
                <div class="mb-4"><strong>Address</strong></div>
                <div>
                    <span>Street</span>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block px-4"></div>
                    <span>Unit #</span>
                    <div style="height: 18px; width: 30px;" class="bg-blue inline-block px-4"></div>
                    <span>City</span>
                    <div style="height: 18px; width: 100px;" class="bg-blue inline-block px-4"></div>
                    <span>State</span>
                    <div style="height: 18px; width: 20px;" class="bg-blue inline-block px-4"></div>
                    <span>ZIP</span>
                    <div style="height: 18px; width: 70px;" class="bg-blue inline-block px-4"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td rowspan="2">
                <strong>Property Value</strong>
            </td>
            <td rowspan="2">
                <strong>Status:</strong>
                <span>Sold, Pending <br>Sale, or Retained</span>
            </td>
            <td rowspan="2">
                <strong>Monthly Insurance, Taxes, Association Dues, etc.</strong><br>
                <em>if not included in <br>Monthly Mortgage Payment</em>
            </td>
            <td colspan="2" class="text-center">
                <strong>For Investment property Only</strong>
            </td>
        </tr>
        <tr>
            <td><strong>Monthly Rental Income</div></td>
            <td>
                <strong>For LENDER to calculate:</strong><br>
                <span>Net Monthly Rental Income</span>
            </td>
        </tr>
        <tr>
            <td>$</td>
            <td></td>
            <td>$</td>
            <td>$</td>
            <td>$</td>
        </tr>
    </table>
    <div style="font-size: 10px;">
        <div class="inline-block font-bold">Mortgage Loans on this Property</div>
        <div class="inline-block ml-8">
            <input type="checkbox" class="align-bottom">
            <span class="font-bold italic">Does not apply</span>
        </div>
    </div>
    <table style="font-size: 11px;" class="bordered mb-20" cellspacing="0">
        <tr>
            <td><strong>Creditor Name</strong></td>
            <td><strong>Account Number</strong></td>
            <td><strong>Monthly <br>Mortgage <br>Payment</strong></td>
            <td>
                <table cellspacing="0">
                    <tr>
                        <td class="border-none p-0"><strong>Unpaid Balance</strong></td>
                        <td class="border-none p-0"><em>(To be paid off at<br>or before closing)</em></td>
                    </tr>
                </table>
            </td>
            <td>
                <strong>Type:</strong>
                <span>FHA, VA, <br>Conventional, <br>USDA-RD, Other</span>
            </td>
            <td>
                <strong>Credit Limit</strong><br>
                <em>(if applicable)</em>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>$</td>
            <td><table cellspacing="0">
                <tr>
                    <td class="border-none">$</td>
                    <td class="border-none"><input style="line-height: 0.7;" type="checkbox" class="inline-block align-bottom"></td>
                </tr>
            </table></td>
            <td></td>
            <td>$</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>$</td>
            <td><table cellspacing="0">
                <tr>
                    <td class="border-none">$</td>
                    <td class="border-none"><input style="line-height: 0.7;" type="checkbox" class="inline-block align-bottom"></td>
                </tr>
            </table></td>
            <td></td>
            <td>$</td>
        </tr>
    </table>

    <div class="border-bottom">
        <div style="font-size: 12px;" class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold">
            3c. IF APPLICABLE, Complete Information for Additional Property
        </div>
        <div style="font-size: 11px;" class="inline-block ml-8">
            <input type="checkbox" class="align-bottom">
            <span class="font-bold italic">Does not apply</span>
        </div>
    </div>

    <table style="font-size: 11px;" class="bordered" cellspacing="0">
        <tr>
            <td colspan="5">
                <div class="mb-4"><strong>Address</strong></div>
                <div>
                    <span>Street</span>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block px-4"></div>
                    <span>Unit #</span>
                    <div style="height: 18px; width: 30px;" class="bg-blue inline-block px-4"></div>
                    <span>City</span>
                    <div style="height: 18px; width: 100px;" class="bg-blue inline-block px-4"></div>
                    <span>State</span>
                    <div style="height: 18px; width: 20px;" class="bg-blue inline-block px-4"></div>
                    <span>ZIP</span>
                    <div style="height: 18px; width: 70px;" class="bg-blue inline-block px-4"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td rowspan="2">
                <strong>Property Value</strong>
            </td>
            <td rowspan="2">
                <strong>Status:</strong>
                <span>Sold, Pending <br>Sale, or Retained</span>
            </td>
            <td rowspan="2">
                <strong>Monthly Insurance, Taxes, Association Dues, etc.</strong><br>
                <em>if not included in <br>Monthly Mortgage Payment</em>
            </td>
            <td colspan="2" class="text-center">
                <strong>For Investment property Only</strong>
            </td>
        </tr>
        <tr>
            <td><strong>Monthly Rental Income</div></td>
            <td>
                <strong>For LENDER to calculate:</strong><br>
                <span>Net Monthly Rental Income</span>
            </td>
        </tr>
        <tr>
            <td>$</td>
            <td></td>
            <td>$</td>
            <td>$</td>
            <td>$</td>
        </tr>
    </table>
    <div style="font-size: 10px;">
        <div class="inline-block font-bold">Mortgage Loans on this Property</div>
        <div class="inline-block ml-8">
            <input type="checkbox" class="align-bottom">
            <span class="font-bold italic">Does not apply</span>
        </div>
    </div>
    <table style="font-size: 11px;" class="bordered mb-20" cellspacing="0">
        <tr>
            <td><strong>Creditor Name</strong></td>
            <td><strong>Account Number</strong></td>
            <td><strong>Monthly <br>Mortgage <br>Payment</strong></td>
            <td>
                <table cellspacing="0">
                    <tr>
                        <td class="border-none p-0"><strong>Unpaid Balance</strong></td>
                        <td class="border-none p-0"><em>(To be paid off at<br>or before closing)</em></td>
                    </tr>
                </table>
            </td>
            <td>
                <strong>Type:</strong>
                <span>FHA, VA, <br>Conventional, <br>USDA-RD, Other</span>
            </td>
            <td>
                <strong>Credit Limit</strong><br>
                <em>(if applicable)</em>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>$</td>
            <td><table cellspacing="0">
                <tr>
                    <td class="border-none">$</td>
                    <td class="border-none"><input style="line-height: 0.7;" type="checkbox" class="inline-block align-bottom"></td>
                </tr>
            </table></td>
            <td></td>
            <td>$</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>$</td>
            <td><table cellspacing="0">
                <tr>
                    <td class="border-none">$</td>
                    <td class="border-none"><input style="line-height: 0.7;" type="checkbox" class="inline-block align-bottom"></td>
                </tr>
            </table></td>
            <td></td>
            <td>$</td>
        </tr>
    </table>
    <hr>
    <div style="font-size: 12px;" class="mt-8">
        <strong>Borrower Name:</strong>
        <div style="height: 18px; width: 150px;" class="bg-blue inline-block px-4 mr-10"></div>
    </div>
    <div style="font-size: 10px;" class="pt-4">
        <div>Uniform Residential Loan Application</div>
        <div>Freddie Mac Form 65 • Fannie Mae Form 1003</div>
        <div><em>Effective 07/2019</em></div>
    </div>
</body>
</html>
