<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/pdf.css">
</head>
<body>
    <div style="font-size: 12px;" class="border-bottom">
        <div class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold mr-8">
            1b. Current Employment/Self Employment and Income
        </div>
        <input type="checkbox" class="align-middle">
        <div class="inline-block"><strong>Does not apply</strong></div>
    </div>

    <table style="font-size: 11px;" class="bordered mb-20" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="2">
                <div class="mb-4">
                    <strong>Employer or Business Name</strong>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>Phone</span>
                    (<div style="height: 18px; width: 30px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>)
                    <div style="height: 18px; width: 30px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>-
                    <div style="height: 18px; width: 60px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                </div>
                <div class="mb-4">
                    <span>Street</span>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                </div>
                <div class="mb-4">
                    <span>City</span>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>State</span>
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>ZIP</span>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                </div>
            </td>
            <td rowspan="3" style="width: 177px; font-size: 10px;">
                <div style="font-size: 11px;" class="font-bold mb-4">Gross Monthly Income</div>
                <div class="mb-4">
                    <span style="width: 70px;" class="inline-block">Base</span>
                    <span class="inline-block">$</span>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>/month</span>
                </div>
                <div class="mb-4">
                    <span style="width: 70px;" class="inline-block">Overtime</span>
                    <span class="inline-block">$</span>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>/month</span>
                </div>
                <div class="mb-4">
                    <span style="width: 70px;" class="inline-block">Bonus</span>
                    <span class="inline-block">$</span>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>/month</span>
                </div>
                <div class="mb-4">
                    <span style="width: 70px;" class="inline-block">Commission</span>
                    <span class="inline-block">$</span>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>/month</span>
                </div>
                <div class="mb-4">
                    <span style="width: 70px;" class="inline-block">Military<br>Entitlements</span>
                    <span class="inline-block">$</span>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>/month</span>
                </div>
                <div class="mb-4">
                    <span style="width: 70px;" class="inline-block">Other</span>
                    <span class="inline-block">$</span>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>/month</span>
                </div>
                <div>
                    <span style="width: 70px;" class="inline-block font-bold">TOTAL</span>
                    <span class="inline-block">$</span>
                    <div style="height: 18px; width: 50px;" class="inline-block align-middle px-4 leading-normal"></div>
                    <span class="font-bold">/month</span>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="mb-4">
                    <strong>Position or Title</strong>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                </div>
                <div>
                    <strong>Start Date</strong>
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>/
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div><em>(mm/yyyy)</em>
                </div>
            </td>
            <td>
                <div class="font-bold">Check if this statement applies:</div>
                <div>
                    <input type="checkbox" class="align-bottom">
                    <span style="font-size: 10px;">I am employed by a family member, property seller, real estate agent, or other party to the transaction.</span>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div>
                    <input type="checkbox" class="align-middle">
                    <span style="width: 160px;" class="inline-block font-bold">Check if you are the Business</span>
                    <input type="radio" class="align-middle">
                    <span style="width: 200px;" class="inline-block">I have ownership share of less than 25%</span>
                    <span class="inline-block font-bold">Monthly Income (or Loss)</span>
                </div>
                <div>
                    <span style="width: 15px;" class="inline-block"></span>
                    <span style="width: 160px;" class="inline-block font-bold">Owner or or Self-Employed</span>
                    <input type="radio" class="align-middle">
                    <span style="width: 200px;" class="inline-block">I have ownership share of 25% or more</span>
                    <span class="inline-block">$</span>
                    <div style="height: 18px; width: 100px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                </div>
            </td>
        </tr>
    </table>

    <div style="font-size: 12px;" class="border-bottom">
        <div class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold mr-8">
            1c. IF APPLICABLE, Complete Information for Additional Employment/Self Employment and Income
        </div>
        <input type="checkbox" class="align-middle">
        <div class="inline-block"><strong>Does not apply</strong></div>
    </div>

    <table style="font-size: 11px;" class="bordered mb-20" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="2">
                <div class="mb-4">
                    <strong>Employer or Business Name</strong>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>Phone</span>
                    (<div style="height: 18px; width: 30px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>)
                    <div style="height: 18px; width: 30px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>-
                    <div style="height: 18px; width: 60px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                </div>
                <div class="mb-4">
                    <span>Street</span>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                </div>
                <div class="mb-4">
                    <span>City</span>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>State</span>
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>ZIP</span>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                </div>
            </td>
            <td rowspan="3" style="width: 177px; font-size: 10px;">
                <div style="font-size: 11px;" class="font-bold mb-4">Gross Monthly Income</div>
                <div class="mb-4">
                    <span style="width: 70px;" class="inline-block">Base</span>
                    <span class="inline-block">$</span>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>/month</span>
                </div>
                <div class="mb-4">
                    <span style="width: 70px;" class="inline-block">Overtime</span>
                    <span class="inline-block">$</span>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>/month</span>
                </div>
                <div class="mb-4">
                    <span style="width: 70px;" class="inline-block">Bonus</span>
                    <span class="inline-block">$</span>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>/month</span>
                </div>
                <div class="mb-4">
                    <span style="width: 70px;" class="inline-block">Commission</span>
                    <span class="inline-block">$</span>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>/month</span>
                </div>
                <div class="mb-4">
                    <span style="width: 70px;" class="inline-block">Military<br>Entitlements</span>
                    <span class="inline-block">$</span>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>/month</span>
                </div>
                <div class="mb-4">
                    <span style="width: 70px;" class="inline-block">Other</span>
                    <span class="inline-block">$</span>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>/month</span>
                </div>
                <div>
                    <span style="width: 70px;" class="inline-block font-bold">TOTAL</span>
                    <span class="inline-block">$</span>
                    <div style="height: 18px; width: 50px;" class="inline-block align-middle px-4 leading-normal"></div>
                    <span class="font-bold">/month</span>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="mb-4">
                    <strong>Position or Title</strong>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                </div>
                <div>
                    <strong>Start Date</strong>
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>/
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div><em>(mm/yyyy)</em>
                </div>
            </td>
            <td>
                <div class="font-bold">Check if this statement applies:</div>
                <div>
                    <input type="checkbox" class="align-bottom">
                    <span style="font-size: 10px;">I am employed by a family member, property seller, real estate agent, or other party to the transaction.</span>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div>
                    <input type="checkbox" class="align-middle">
                    <span style="width: 160px;" class="inline-block font-bold">Check if you are the Business</span>
                    <input type="radio" class="align-middle">
                    <span style="width: 200px;" class="inline-block">I have ownership share of less than 25%</span>
                    <span class="inline-block font-bold">Monthly Income (or Loss)</span>
                </div>
                <div>
                    <span style="width: 15px;" class="inline-block"></span>
                    <span style="width: 160px;" class="inline-block font-bold">Owner or or Self-Employed</span>
                    <input type="radio" class="align-middle">
                    <span style="width: 200px;" class="inline-block">I have ownership share of 25% or more</span>
                    <span class="inline-block">$</span>
                    <div style="height: 18px; width: 100px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                </div>
            </td>
        </tr>
    </table>

    <div style="font-size: 12px;">
        <div class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold mr-8">
            1d. IF APPLICABLE, Complete Information for Previous Employment/Self Employment and Income
        </div>
        <input type="checkbox" class="align-middle">
        <div class="inline-block"><strong>Does not apply</strong></div>
    </div>
    <div style="font-size: 11px;" class="border-top font-bold py-4">
        Provide at least 2 years of current and previous employment and income.
    </div>

    <table style="font-size: 11px;" class="bordered mb-20" cellspacing="0" cellpadding="0">
        <tr>
            <td style="width: 58%;">
                <div class="mb-4">
                    <strong>Employer or Business Name</strong>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                </div>
                <div class="mb-4">
                    <span>Street</span>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                </div>
                <div class="mb-4">
                    <span>City</span>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>State</span>
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                    <span>ZIP</span>
                    <div style="height: 18px; width: 50px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                </div>
                <div class="mb-4">
                    <strong>Position or Title</strong>
                    <div style="height: 18px; width: 150px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
                </div>
                <div>
                    <strong>Start Date</strong>
                    <div style="height: 18px; width: 30px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>/
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div><em>(mm/yyyy)</em>
                    <div style="width: 20px;" class="inline-block"></div>
                    <strong>End Date</strong>
                    <div style="height: 18px; width: 30px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>/
                    <div style="height: 18px; width: 40px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div><em>(mm/yyyy)</em>
                </div>
            </td>
            <td>
                <input type="checkbox" class="align-bottom">
                <span class="font-bold">Check if you were the Business Owner or Self-Employed</span>
            </td>
            <td>
                <div class="font-bold">Previous Gross Monthly Income</div>
                <span>$</span>
                <div style="height: 18px; width: 120px;" class="bg-blue inline-block align-middle px-4 leading-normal"></div>
            </td>
        </tr>
    </table>

    <div style="font-size: 12px;">
        <div class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold mr-8">
            1e. Income from Other Sources
        </div>
        <input type="checkbox" class="align-middle">
        <div class="inline-block"><strong>Does not apply</strong></div>
    </div>
    <div style="font-size: 11px;" class="border-top">
        <div class="font-bold">
            Include income from other sources below. Under Income Source, choose from the sources listed here:
        </div>
        <table style="font-size: 10px;" cellpadding="0">
            <tr>
                <td>
                    <div>• Alimony</div>
                    <div>• Automobile Allowance</div>
                    <div>• Boarder Income</div>
                    <div>• Capital Gains</div>
                </td>
                <td>
                    <div>• Child Support</div>
                    <div>• Disability</div>
                    <div>• Foster Care</div>
                    <div>• Housing or Parsonage</div>
                </td>
                <td>
                    <div>• Interest and Dividends</div>
                    <div>• Mortgage Credit Certificate</div>
                    <div>• Mortgage Differential<br>Payments</div>
                </td>
                <td>
                    <div>• Notes Receivable</div>
                    <div>• Public Assistance</div>
                    <div>• Retirement <br><em>(e.g., Pension, IRA)</em></div>
                </td>
                <td>
                    <div>• Royalty Payments</div>
                    <div>• Separate Maintenance</div>
                    <div>• Social Security</div>
                    <div>• Trust</div>
                </td>
                <td>
                    <div>• Unemployment<br>Benefits</div>
                    <div>• VA Compensation</div>
                    <div>• Other</div>
                </td>
            </tr>
        </table>
        <div>
            <strong>NOTE:</strong>
            <em>Reveal alimony, child support, separate maintenance, or other come ONLY IF you want it considered in determining your qualification for this loan.</em>
        </div>
    
        <table style="font-size: 11px;" class="bordered" cellpadding="0" cellspacing="0">
            <tr>
                <td><strong>Income Source</strong></td>
                <td><strong>Monthly Income</strong></td>
            </tr>
            <tr>
                <td></td>
                <td>$</td>
            </tr>
            <tr>
                <td></td>
                <td>$</td>
            </tr>
            <tr>
                <td></td>
                <td>$</td>
            </tr>
            <tr>
                <td><div class="text-right font-bold">Provide TOTAL Amount Here</div></td>
                <td>$</td>
            </tr>
        </table>
    </div>

    <hr style="margin-top: 20px;">
    <div style="font-size: 12px;" class="mt-8">
        <strong>Borrower Name:</strong>
        <div style="height: 18px; width: 150px;" class="bg-blue inline-block px-4 mr-10"></div>
    </div>
    <div style="font-size: 10px;" class="pt-4">
        <div>Uniform Residential Loan Application</div>
        <div>Freddie Mac Form 65 • Fannie Mae Form 1003</div>
        <div><em>Effective 07/2019</em></div>
    </div>

</body>
</html>