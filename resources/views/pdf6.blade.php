<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/pdf.css">
</head>
<body>
    <hr>
    <div class="mt-4 mb-10">
        <span style="font-size: 16px;" class="font-bold">Section 5: Declarations.</span>
        <span style="font-size: 12px;">This section asks you specific questions about the property, your funding, and your past financial history.</span>
    </div>

    <div class="border-bottom">
        <div style="font-size: 12px;" class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold">
            5a. About this Property and Your Money for this Loan
        </div>
    </div>

    <table style="font-size: 11px;" class="bordered mb-20" cellspacing="0">
        <tr>
            <td style="width: 85%;">
                <div>
                    <div class="inline-block"><strong>A.</strong> Will you occupy the property as your primary residence?</div>
                </div>
                <div>
                    <div style="width: 15px;" class="inline-block font-bold"></div>
                    <div class="inline-block">If YES, have you had an ownership interest in another property in the last three years?</div>
                </div>
                <div class="mb-8">
                    <div style="width: 25px;" class="inline-block font-bold"></div>
                    <div class="inline-block">If YES, complete (1) and (2) below:</div>
                </div>
                <div>
                    <div style="width: 25px;" class="inline-block font-bold"></div>
                    <div class="inline-block">(1) What type of property did you own: primary residence (PR), FHA secondary residence (SR), second home (SH), or investment property (IP)?</div>
                </div>
                <div>
                    <div style="width: 25px;" class="inline-block font-bold"></div>
                    <div class="inline-block">(2) How did you hold title to the property: by yourself (S), jointly with your spouse (SP), or jointly with another person? (O)?</div>
                </div>
            </td>
            <td class="align-top">
                <div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block mr-8">NO</div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block">YES</div>
                </div>
                <div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block mr-8">NO</div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block">YES</div>
                </div>
                <div style="margin-top: 13px;">
                    <div style="height: 18px; width: 80px;" class="bg-blue inline-block px-4"></div>
                </div>
                <div style="margin-top: 17px;">
                    <div style="height: 18px; width: 80px;" class="bg-blue inline-block px-4"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <div class="inline-block"><strong>B.</strong> If this is a Purchase Transaction: Do you have a family relationship or business affiliation with the seller of the property?</div>
                </div>
            </td>
            <td>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block mr-8">NO</div>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block">YES</div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="pt-4">
                    <div class="inline-block">
                        <strong>C.</strong>
                        <span>Are you borrowing any money for this real estate transaction (eg, money for your closing costs or down payment) or obtaining any money from another party, such as the seller or realtor, that you have not disclosed on this loan application?</span>
                    </div>
                    <div class="inline-block"></div>
                </div>
                <div>
                    <div style="width: 20px;" class="inline-block font-bold"></div>
                    <div class="inline-block">IF YES, what is the amount of this money?</div>
                </div>
            </td>
            <td>
                <div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block mr-8">NO</div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block">YES</div>
                </div>
                <div class="mt-4">
                    <div style="height: 18px; width: 60px;" class="bg-blue inline-block px-4">$</div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <strong>D.</strong>
                    <span>1. Have you or will you be applying for a mortgage loan on another property (not the property securing this loan) on or before closing this transaction that is not disclosed on this loan application?</span>
                </div>
                <div>
                    <div style="width: 12px;" class="inline-block"></div>
                    <span>2. Have you or will you be applying for any new credit (e.g., installment loan, credit card, etc.) on or before closing this loan that is not disclosed on this application?</span>
                </div>
            </td>
            <td class="align-top">
                <div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block mr-8">NO</div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block">YES</div>
                </div>
                <div class="mt-8">
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block mr-8">NO</div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block">YES</div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <strong>E.</strong>
                    <span>Will this property be subjected to a lien that could take priority over the first mortgage lien, such as a clean energy lien paid through your property taxes <em>(e.g., the Property Assessed Clean Energy Program)?</em></span>
                </div>
            </td>
            <td>
                <div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block mr-8">NO</div>
                    <input style="font-size: 14px;" type="radio">
                    <div class="inline-block">YES</div>
                </div>
            </td>
        </tr>
    </table>

    <div class="border-bottom">
        <div style="font-size: 12px;" class="inline-block bg-black text-white border-radius-top px-10 py-2 font-bold">
            5b. About Your Finances
        </div>
    </div>

    <table style="font-size: 11px;" class="bordered mb-20" cellspacing="0">
        <tr>
            <td style="width: 85%;">
                <div>
                    <div class="inline-block"><strong>F.</strong> Are you a co-signer or guarantor on any debt or loan that is not disclosed on this application?</div>
                </div>
            </td>
            <td>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block mr-8">NO</div>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block">YES</div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <div class="inline-block"><strong>G.</strong> Are there any outstanding judgments against you?</div>
                </div>
            </td>
            <td>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block mr-8">NO</div>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block">YES</div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <div class="inline-block"><strong>H.</strong> Are you currently delinquent or in default on a federal debt?</div>
                </div>
            </td>
            <td>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block mr-8">NO</div>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block">YES</div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <div class="inline-block"><strong>I.</strong> Are you a party to a lawsuit in which you potentially have any personal financial liability?</div>
                </div>
            </td>
            <td>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block mr-8">NO</div>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block">YES</div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <div class="inline-block"><strong>J.</strong> Have you conveyed title to any property in lieu of foreclosure in the past 7 years?</div>
                </div>
            </td>
            <td>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block mr-8">NO</div>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block">YES</div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="pt-4">
                    <div class="inline-block"><strong>K.</strong> Within the past 7 years, have you completed a pre-foreclosure sale or short sale, whereby the property was sold to a third party and the Lender agreed to accept less than the outstanding mortgage balance due?</div>
                </div>
            </td>
            <td>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block mr-8">NO</div>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block">YES</div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <div class="inline-block"><strong>L.</strong> Have you had property foreclosed upon in the last 7 years?</div>
                </div>
            </td>
            <td>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block mr-8">NO</div>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block">YES</div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <div class="inline-block"><strong>M.</strong> Have you declared bankruptcy within the past 7 years?</div>
                </div>
                <div>
                    <div style="width: 14px;" class="inline-block"></div>
                    <div class="inline-block">IF YES, identify the type(s) of bankruptcy: </div>
                    <input type="checkbox" class="align-middle">
                    <div class="inline-block mr-10">Chapter 7</div>
                    <input type="checkbox" class="align-middle">
                    <div class="inline-block mr-10">Chapter 11</div>
                    <input type="checkbox" class="align-middle">
                    <div class="inline-block mr-10">Chapter 12</div>
                    <input type="checkbox" class="align-middle">
                    <div class="inline-block">Chapter 13</div>
                </div>
            </td>
            <td>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block mr-8">NO</div>
                <input style="font-size: 14px;" type="radio">
                <div class="inline-block">YES</div>
            </td>
        </tr>
    </table>

    <hr style="margin-top: 300px;">
    <div style="font-size: 12px;" class="mt-8">
        <strong>Borrower Name:</strong>
        <div style="height: 18px; width: 150px;" class="bg-blue inline-block px-4 mr-10"></div>
    </div>
    <div style="font-size: 10px;" class="pt-4">
        <div>Uniform Residential Loan Application</div>
        <div>Freddie Mac Form 65 • Fannie Mae Form 1003</div>
        <div><em>Effective 07/2019</em></div>
    </div>
</body>
</html>