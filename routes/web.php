<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/downloadpdf','DownloadPDFController@export_pdf');
Route::get('/downloadpdf2','DownloadPDFController@export_pdf2');
Route::get('/downloadpdf3','DownloadPDFController@export_pdf3');
Route::get('/downloadpdf4','DownloadPDFController@export_pdf4');
Route::get('/downloadpdf5','DownloadPDFController@export_pdf5');
Route::get('/downloadpdf6','DownloadPDFController@export_pdf6');
Route::get('/downloadpdf7','DownloadPDFController@export_pdf7');
Route::get('/downloadpdf8','DownloadPDFController@export_pdf8');