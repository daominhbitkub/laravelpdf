<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DownloadPDFController extends Controller
{
    //
    public function export_pdf()
    {
        
        $pdf = \PDF::loadView('pdf');
        
        
        return $pdf->stream();
    }

    public function export_pdf2()
    {
        
        $pdf2 = \PDF::loadView('pdf2');
        
        
        return $pdf2->stream();
    }

    public function export_pdf3()
    {
        
        $pdf3 = \PDF::loadView('pdf3');
        
        
        return $pdf3->stream();
    }

    public function export_pdf4()
    {
        
        $pdf4 = \PDF::loadView('pdf4');
        
        
        return $pdf4->stream();
    }

    public function export_pdf5()
    {
        
        $pdf5 = \PDF::loadView('pdf5');
        
        
        return $pdf5->stream();
    }

    public function export_pdf6()
    {
        
        $pdf6 = \PDF::loadView('pdf6');
        
        
        return $pdf6->stream();
    }

    public function export_pdf7()
    {
        
        $pdf7 = \PDF::loadView('pdf7');
        
        
        return $pdf7->stream();
    }

    public function export_pdf8()
    {
        
        $pdf8 = \PDF::loadView('pdf8');
        
        
        return $pdf8->stream();
    }
}
